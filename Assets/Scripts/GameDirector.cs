﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public class GameDirector : MonoBehaviour
    {
        [Header("Managers")]
        public FadeTransitionManager fadeTransitionManager;
        public UIManager uIManager;

        [Space(30)]
        public List<GameModesData> modesData;

        private GameModesData currentModeData;

        private float serverHealth;

        private void Start()
        {
            SetupServerHealth(true, true);
            SetupNewModeOpeningText();
        }

        public void SetupNewModeOpeningText()
        {
            SetupServerHealth(false, false);

            int randomGameMode = Random.Range(0, modesData.Count);
            currentModeData = modesData[randomGameMode];
            uIManager.ShowOpeningDialouge();
            uIManager.SetOpeningText(currentModeData.GetRandomOpeningLine());
        }

        public void StartGameMode()
        {
            StartCoroutine(StartGameTransition());
        }

        private IEnumerator StartGameTransition()
        {
            fadeTransitionManager.FadeIn();

            yield return new WaitUntil(() => fadeTransitionManager.fadeTweenComplete);

            uIManager.gameObject.SetActive(false);
            uIManager.HideOpeningDialouge();
            fadeTransitionManager.FadeOut();
            currentModeData.modeObject.SetActive(true);

            yield return new WaitUntil(() => fadeTransitionManager.fadeTweenComplete);
            StartGameModeModule();
        }

        private void StartGameModeModule()
        {
            switch (currentModeData.mode)
            {
                case GAME_MODES.BUGS_KILL:
                    {
                        currentModeData.modeObject.GetComponent<BugsKill.GameManager>().StartGame();
                        break;
                    }
                case GAME_MODES.ELECTRICITY:
                    {
                        currentModeData.modeObject.GetComponent<Electricity.GameManager>().StartGame();
                        break;
                    }
            }
        }

        public void GameModeModuleOver(float percentagePerformance)
        {
            int healthIncrease = (int)((percentagePerformance / 100) * 20);
            StartCoroutine(StopGameTransition(healthIncrease));
        }

        private IEnumerator StopGameTransition(int percentagePerformance)
        {
            fadeTransitionManager.FadeIn();

            yield return new WaitUntil(() => fadeTransitionManager.fadeTweenComplete);

            currentModeData.modeObject.SetActive(false);
            fadeTransitionManager.FadeOut();
            uIManager.ShowEndingDialouge();
            uIManager.gameObject.SetActive(true);

            yield return new WaitUntil(() => fadeTransitionManager.fadeTweenComplete);
            uIManager.SetClosingText(currentModeData.GetRandomClosingLine());

            SetupServerHealth(false, true, percentagePerformance);

            yield return new WaitForSeconds(2);

            uIManager.HideEndingDialouge();
            SetupNewModeOpeningText();
        }

        private void SetupServerHealth(bool initial, bool increase, int increaseValue = 0)
        {
            if (initial)
            {
                serverHealth = 100;
                uIManager.SetServerHealth(serverHealth / 100, true);
            }
            else
            {
                if (!increase)
                {
                    serverHealth -= 20;
                    uIManager.SetServerHealth(serverHealth / 100, false);
                }
                else
                {
                    serverHealth += increaseValue;
                    uIManager.SetServerHealth(serverHealth / 100, true);
                }
            }
        }
    }

    [System.Serializable]
    public struct GameModesData
    {
        public GAME_MODES mode;
        public GameObject modeObject;
        public List<string> openingLines;
        public List<string> closingLines;

        public string GetRandomOpeningLine()
        {
            return openingLines[Random.Range(0, openingLines.Count)];
        }

        public string GetRandomClosingLine()
        {
            return closingLines[Random.Range(0, closingLines.Count)];
        }
    }
}
