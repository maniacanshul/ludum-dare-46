﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public List<AudioSource> sfxSources;

    public List<SFXData> sfxData;

    public static AudioHandler instance;

    private List<AudioSource> sfxSourcesBeingUsed;

    private void Awake()
    {
        instance = this;
        sfxSourcesBeingUsed = new List<AudioSource>();
    }

    public void PlaySFX(AUDIO_SFX sfx)
    {
        for (int i = 0; i < sfxData.Count; i++)
        {
            if (sfxData[i].sfxType == sfx)
            {
                PlaySFX(sfxData[i].clip);
                break;
            }
        }
    }

    private void PlaySFX(AudioClip clip)
    {
        DespawnSFXSource();

        AudioSource temp = sfxSources[0];
        sfxSources.Remove(temp);
        sfxSourcesBeingUsed.Add(temp);

        sfxSources[0].clip = clip;
        if (clip != null)
        {
            sfxSources[0].Play();
        }
    }

    private void DespawnSFXSource()
    {
        for (int i = sfxSourcesBeingUsed.Count - 1; i >= 0; i--)
        {
            if (!sfxSourcesBeingUsed[i].isPlaying)
            {
                sfxSources.Add(sfxSourcesBeingUsed[i]);
                sfxSourcesBeingUsed.Remove(sfxSourcesBeingUsed[i]);
            }
        }
    }
}

[System.Serializable]
public class SFXData
{
    public AudioClip clip;
    public AUDIO_SFX sfxType;
}

public enum AUDIO_SFX
{

}