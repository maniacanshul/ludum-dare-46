﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BugsKill
{
    public class EnemyManager : MonoBehaviour
    {
        public List<GameObject> enemyObjects;

        private List<GameObject> enemyBeingUsedLeft;
        private List<GameObject> enemyBeingUsedRight;
        private List<GameObject> enemyBeingUsedTop;
        private List<GameObject> enemyBeingUsedBottom;

        public float spawnTimeInterval;
        public float movementSpeed;

        [Header("Spawner details")]
        public Transform left1;
        public Transform left2, right1, right2;
        public Transform top1, top2;
        public Transform bottom1, bottom2;

        private float resetThreshold = 5;
        private bool moveEnemies;
        private int numberOfEnemiesSpawned;

        public void StartSpawner()
        {
            enemyBeingUsedLeft = new List<GameObject>();
            enemyBeingUsedRight = new List<GameObject>();
            enemyBeingUsedBottom = new List<GameObject>();
            enemyBeingUsedTop = new List<GameObject>();

            StartCoroutine(Spawner());
        }

        private IEnumerator Spawner()
        {
            moveEnemies = true;
            numberOfEnemiesSpawned = 0;

            while (true)
            {
                yield return new WaitForSeconds(spawnTimeInterval);
                SpawnEnemy();
            }
        }

        private void SpawnEnemy()
        {
            if (enemyObjects.Count <= 0)
            {
                return;
            }
            numberOfEnemiesSpawned++;

            GameObject tempEnemy = enemyObjects[0];
            int randomNum = Random.Range(0, 101);

            if (randomNum < 25)
            {
                SpawnFromLeft(tempEnemy);
            }
            else if (randomNum >= 25 && randomNum < 50)
            {
                SpawnFromRight(tempEnemy);
            }
            else if (randomNum >= 50 && randomNum < 75)
            {
                SpawnFromTop(tempEnemy);
            }
            else if (randomNum >= 75)
            {
                SpawnFromBottom(tempEnemy);
            }
        }

        private void SpawnFromLeft(GameObject tempEnemy)
        {
            float y = Random.Range(left2.position.y, left1.position.y);
            float x = left1.position.x;

            Vector3 pos = tempEnemy.transform.position;
            pos.x = x;
            pos.y = y;
            tempEnemy.transform.position = pos;

            Vector3 rotation = new Vector3(0, 0, -90);
            tempEnemy.transform.localEulerAngles = rotation;

            enemyObjects.Remove(tempEnemy);
            enemyBeingUsedLeft.Add(tempEnemy);
        }

        private void SpawnFromRight(GameObject tempEnemy)
        {
            float y = Random.Range(right2.position.y, right1.position.y);
            float x = right1.position.x;

            Vector3 pos = tempEnemy.transform.position;
            pos.x = x;
            pos.y = y;
            tempEnemy.transform.position = pos;

            Vector3 rotation = new Vector3(0, 0, 90);
            tempEnemy.transform.localEulerAngles = rotation;

            enemyObjects.Remove(tempEnemy);
            enemyBeingUsedRight.Add(tempEnemy);
        }

        private void SpawnFromTop(GameObject tempEnemy)
        {
            float x = Random.Range(top2.position.y, top1.position.y);
            float y = top1.position.y;

            Vector3 pos = tempEnemy.transform.position;
            pos.x = x;
            pos.y = y;
            tempEnemy.transform.position = pos;

            Vector3 rotation = new Vector3(180, 0, 0);
            tempEnemy.transform.localEulerAngles = rotation;

            enemyObjects.Remove(tempEnemy);
            enemyBeingUsedTop.Add(tempEnemy);
        }

        private void SpawnFromBottom(GameObject tempEnemy)
        {
            float x = Random.Range(bottom2.position.y, bottom1.position.y);
            float y = bottom1.position.y;

            Vector3 pos = tempEnemy.transform.position;
            pos.x = x;
            pos.y = y;
            tempEnemy.transform.position = pos;

            Vector3 rotation = new Vector3(0, 0, 0);
            tempEnemy.transform.localEulerAngles = rotation;

            enemyObjects.Remove(tempEnemy);
            enemyBeingUsedBottom.Add(tempEnemy);
        }

        private void Update()
        {
            if (!moveEnemies)
            {
                return;
            }

            for (int i = enemyBeingUsedLeft.Count - 1; i >= 0; i--)
            {
                Vector3 tempPos = enemyBeingUsedLeft[i].transform.position;
                tempPos.x += movementSpeed * Time.deltaTime;
                enemyBeingUsedLeft[i].transform.position = tempPos;

                if (tempPos.x > right1.transform.position.x + 5)
                {
                    enemyObjects.Add(enemyBeingUsedLeft[i]);
                    enemyBeingUsedLeft.Remove(enemyBeingUsedLeft[i]);
                }
            }

            for (int i = enemyBeingUsedRight.Count - 1; i >= 0; i--)
            {
                Vector3 tempPos = enemyBeingUsedRight[i].transform.position;
                tempPos.x -= movementSpeed * Time.deltaTime;
                enemyBeingUsedRight[i].transform.position = tempPos;

                if (tempPos.x < left1.transform.position.x - 5)
                {
                    enemyObjects.Add(enemyBeingUsedRight[i]);
                    enemyBeingUsedRight.Remove(enemyBeingUsedRight[i]);
                }
            }

            for (int i = enemyBeingUsedTop.Count - 1; i >= 0; i--)
            {
                Vector3 tempPos = enemyBeingUsedTop[i].transform.position;
                tempPos.y -= movementSpeed * Time.deltaTime;
                enemyBeingUsedTop[i].transform.position = tempPos;

                if (tempPos.y < bottom1.transform.position.y - 5)
                {
                    enemyObjects.Add(enemyBeingUsedTop[i]);
                    enemyBeingUsedTop.Remove(enemyBeingUsedTop[i]);
                }
            }

            for (int i = enemyBeingUsedBottom.Count - 1; i >= 0; i--)
            {
                Vector3 tempPos = enemyBeingUsedBottom[i].transform.position;
                tempPos.y += movementSpeed * Time.deltaTime;
                enemyBeingUsedBottom[i].transform.position = tempPos;

                if (tempPos.y > top1.transform.position.y + 5)
                {
                    enemyObjects.Add(enemyBeingUsedBottom[i]);
                    enemyBeingUsedBottom.Remove(enemyBeingUsedBottom[i]);
                }
            }
        }

        public void DespawnEnemy(GameObject enemy)
        {
            enemyBeingUsedLeft.Remove(enemy);
            enemyBeingUsedRight.Remove(enemy);
            enemyBeingUsedTop.Remove(enemy);
            enemyBeingUsedBottom.Remove(enemy);

            enemyObjects.Add(enemy);
            Vector3 tempPlacementPos = enemy.transform.position;
            tempPlacementPos.x = 200;
            enemy.transform.position = tempPlacementPos;
        }

        public int GameOver()
        {
            moveEnemies = false;
            StopAllCoroutines();
            return numberOfEnemiesSpawned;
        }
    }
}