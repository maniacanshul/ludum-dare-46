﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace BugsKill
{
    public class PlayerManager : MonoBehaviour
    {
        public Camera gameCam;
        public EnemyManager enemyManager;
        public GameManager gameManager;

        internal bool canPlay;
        private bool isTweenActive;

        private void Update()
        {
            if (!canPlay)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                Smash();
            }

            MoveBasedOnMousePosition();
        }

        private void MoveBasedOnMousePosition()
        {
            if (isTweenActive)
            {
                return;
            }

            Vector3 screenPoint = (Input.mousePosition);
            screenPoint.z = 10.0f;

            Vector3 moveTo = gameCam.ScreenToWorldPoint(screenPoint);
            moveTo.z = transform.localPosition.z;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, moveTo, 0.5f);
        }

        private void Smash()
        {
            float tweenTime = 0.1f;

            isTweenActive = true;

            transform.DOKill();
            transform.DOLocalMoveZ(2, tweenTime);
            transform.DOLocalMoveZ(0, tweenTime).SetDelay(tweenTime).OnComplete(() =>
           {
               isTweenActive = false;
           });
        }

        private void OnTriggerEnter(Collider hit)
        {
            if(!canPlay)
            {
                return;
            }

            if (hit.CompareTag("Enemy"))
            {
                enemyManager.DespawnEnemy(hit.gameObject);
                gameManager.UpdateScore();
            }
        }
    }
}