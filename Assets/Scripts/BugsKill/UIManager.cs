﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace BugsKill
{
    public class UIManager : MonoBehaviour
    {
        public GameManager gameManager;
        public Image timerBar;
        public Text scoreText;

        public void StartTimer(float timer)
        {
            timerBar.DOKill();
            timerBar.DOFillAmount(1, 0);
            timerBar.DOFillAmount(0, timer).SetEase(Ease.Linear).OnComplete(() =>
            {
                gameManager.GameOver();
            });
        }

        public void UpdateScoreText(int score)
        {
            scoreText.text = score.ToString();
        }
    }
}