﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BugsKill
{
    public class GameManager : MonoBehaviour
    {
        public Main.GameDirector gameDirector;
        public EnemyManager enemyManager;
        public PlayerManager playerManager;
        public UIManager uIManager;

        public float levelTime;

        private int score;

        public bool testLocal;

        private void Start()
        {
            if(testLocal)
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            score = 0;
            uIManager.UpdateScoreText(score);
            enemyManager.StartSpawner();
            uIManager.StartTimer(levelTime);

            playerManager.canPlay = true;

        }

        public void UpdateScore()
        {
            score += 5;
            uIManager.UpdateScoreText(score);
        }

        public void GameOver()
        {
            playerManager.canPlay = false;
            float numberOfEnemiesSpawned = enemyManager.GameOver();
            float performancePercentage = ((score / 5.0f) / numberOfEnemiesSpawned) * 100;
            gameDirector.GameModeModuleOver(performancePercentage);
        }
    }
}
