﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Temperature
{
    public class UIManager : MonoBehaviour
    {
        public GameManager gameManager;
        public Image progressBar;
        public Image timerBar;

        public void StartTimer(float time)
        {
            timerBar.DOKill();
            timerBar.DOFillAmount(1, 0);
            timerBar.DOFillAmount(0, time).SetEase(Ease.Linear).OnComplete(() =>
           {
               gameManager.GameOver((1 - progressBar.fillAmount));
           });
        }

        public void SetProgressBarValue(float value)
        {
            progressBar.fillAmount = value;
        }
    }
}
