﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Temperature
{
    public class PlayerManager : MonoBehaviour
    {
        public GameManager gameManager;
        public Transform switchPivot;

        internal bool canPlay;

        private bool rotationIsLeft;

        private void Start()
        {
            rotationIsLeft = true;
        }

        private void Update()
        {
            if (!canPlay)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
            {
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
        }

        private void RotateHandle()
        {
            switchPivot.DOKill();

            Vector3 tempRot = switchPivot.localEulerAngles;
            rotationIsLeft = !rotationIsLeft;

            if (rotationIsLeft)
            {
                tempRot.z = 25;
            }
            else
            {
                tempRot.z = -25;
            }

            switchPivot.DOLocalRotate(tempRot, 0.1f);
        }
    }
}