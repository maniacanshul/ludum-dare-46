﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main
{
    public class FadeTransitionManager : MonoBehaviour
    {
        public Image fadeImage;

        internal bool fadeTweenComplete;

        private float fadetime = 0.25f;

        public void FadeIn()
        {
            fadeTweenComplete = false;

            fadeImage.DOKill();
            fadeImage.DOFade(0, 0);
            fadeImage.DOFade(1, fadetime).OnComplete(() =>
            {
                fadeTweenComplete = true;
            });
        }

        public void FadeOut()
        {
            fadeTweenComplete = false;

            fadeImage.DOKill();
            fadeImage.DOFade(1, 0);
            fadeImage.DOFade(0, fadetime).OnComplete(() =>
            {
                fadeTweenComplete = true;
            });
        }
    }
}
