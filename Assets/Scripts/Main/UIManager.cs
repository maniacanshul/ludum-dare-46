﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main
{
    public class UIManager : MonoBehaviour
    {
        public GameObject openingDialouge, endingDialouge;
        public Text openingText, endingText;

        public Image serverHealthBar;
        public Text serverHealthNumberChange;

        public void SetOpeningText(string text)
        {
            openingText.text = text;
            openingText.gameObject.SetActive(true);
        }

        public void SetClosingText(string text)
        {
            endingText.text = text;
            endingText.gameObject.SetActive(true);
        }

        public void ShowEndingDialouge()
        {
            endingDialouge.SetActive(true);
            endingText.gameObject.SetActive(false);
        }

        public void HideEndingDialouge()
        {
            endingDialouge.SetActive(false);
            endingText.gameObject.SetActive(false);
        }

        public void ShowOpeningDialouge()
        {
            openingDialouge.SetActive(true);
            openingText.gameObject.SetActive(false);
        }

        public void HideOpeningDialouge()
        {
            openingDialouge.SetActive(false);
            openingText.gameObject.SetActive(false);
        }

        public void SetServerHealth(float health, bool increase)
        {
            serverHealthBar.DOKill();
            serverHealthBar.DOFillAmount(health, 0.2f).SetEase(Ease.Linear);
            SetHealthIdentifier(increase);
        }

        private void SetHealthIdentifier(bool increase)
        {
            serverHealthNumberChange.text = "--";
            serverHealthNumberChange.color = Color.red;
            if (increase)
            {
                serverHealthNumberChange.text = "++";
                serverHealthNumberChange.color = Color.green;
            }

            serverHealthNumberChange.DOKill();
            serverHealthNumberChange.DOFade(1, 0);
            serverHealthNumberChange.DOFade(0, 2);
        }
    }
}