﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Electricity
{
    public class GameManager : MonoBehaviour
    {
        public Main.GameDirector gameDirector;
        public UIManager uIManager;
        public PlayerManager playerManager;

        public float time;
        public float progressReductionSpeed;
        public float progressUpdationValue;

        private float progressValue;
        private bool canPlay;

        public bool testLocal;

        private void Start()
        {
            if (testLocal)
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            canPlay = true;
            uIManager.StartTimer(time);
            progressValue = 1;
            uIManager.SetProgressBarValue(progressValue);
            playerManager.canPlay = true;
            playerManager.StartKeySpawn();
        }

        private void Update()
        {
            if (!canPlay)
            {
                return;
            }

            progressValue -= progressReductionSpeed * Time.deltaTime;
            uIManager.SetProgressBarValue(progressValue);
        }

        public void UpdateProgressBarValueOnCorrectType()
        {
            progressValue += progressUpdationValue;
            progressValue = Mathf.Clamp(progressValue, 0, 1);
        }

        public void GameOver(float percentagePerformance)
        {
            canPlay = false;
            playerManager.canPlay = false;
            gameDirector.GameModeModuleOver(percentagePerformance * 100);
        }

    }
}