﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Electricity
{
    public class PlayerManager : MonoBehaviour
    {
        public GameManager gameManager;
        public UIManager uIManager;
        public Transform switchPivot, handPivot;

        public List<QTE_KEYS> keysToPress;

        internal bool canPlay;

        private int lastKeyIndex;
        private QTE_KEYS currentKey;
        private bool rotationIsLeft;

        private void Start()
        {
            rotationIsLeft = true;
            lastKeyIndex = -1;
        }

        public void StartKeySpawn()
        {
            StartCoroutine(GenerateKey());
        }

        private IEnumerator GenerateKey()
        {
            while (canPlay)
            {
                currentKey = keysToPress[Random.Range(0, keysToPress.Count)];
                uIManager.SetInstructionText(currentKey.ToString(), true);
                yield return new WaitForSeconds(2);
            }
        }
        
        private void Update()
        {
            if (!canPlay)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Space) && currentKey == QTE_KEYS.SPACE)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.E) && currentKey == QTE_KEYS.E)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.R) && currentKey == QTE_KEYS.R)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.Z) && currentKey == QTE_KEYS.Z)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.P) && currentKey == QTE_KEYS.P)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.K) && currentKey == QTE_KEYS.K)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.L) && currentKey == QTE_KEYS.L)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
            else if (Input.GetKeyDown(KeyCode.H) && currentKey == QTE_KEYS.H)
            {
                uIManager.SetInstructionText("", false);
                gameManager.UpdateProgressBarValueOnCorrectType();
                RotateHandle();
            }
        }

        private void RotateHandle()
        {
            switchPivot.DOKill();
            handPivot.DOKill();

            Vector3 tempRot = switchPivot.localEulerAngles;
            rotationIsLeft = !rotationIsLeft;

            float xPos = 0;
            if (rotationIsLeft)
            {
                tempRot.z = 25;
                xPos = -0.2f;
            }
            else
            {
                tempRot.z = -25;
                xPos = 1;
            }

            handPivot.DOLocalMoveX(xPos, 0.1f);
            switchPivot.DOLocalRotate(tempRot, 0.1f);
        }

        public enum QTE_KEYS
        {
            SPACE,
            E,
            R,
            Z,
            P,
            K,
            L,
            H
        }
    }
}
